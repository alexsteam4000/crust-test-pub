#!/bin/sh
# Скрипт генерирует 2 проверочных файла с рандомным содержимым.
# Затем выполняет передачу и чтение каждого файла с проверкой md5 - суммы.
# 2 файла используются, чтобы постоянно перетирать тестовый файл на девайсе
# Требует наличия в системе:
# dd
# awk
# scp (ssh)
# sshpass

# Параметры для настройки
IP=10.0.0.5
PORT=2203
USER=admin
PASSWORD=admin
DST_FILENAME=/tmp/test-file

FILE_SIZE=1000M
REPEAT=1000

# Внутренние параметры
GEN1_FILENAME=gen1
GEN2_FILENAME=gen2
RECV_FILENAME=gen-recv

# Создать тестовые файлы
echo "==> Prepare files [$(date "+%y/%m/%d %T")]"
dd if=/dev/urandom of=$GEN1_FILENAME bs=$FILE_SIZE count=1 iflag=fullblock
dd if=/dev/urandom of=$GEN2_FILENAME bs=$FILE_SIZE count=1 iflag=fullblock
GEN1_MD5=$(md5sum $GEN1_FILENAME | awk '{print $1;}')
GEN2_MD5=$(md5sum $GEN2_FILENAME | awk '{print $1;}')
echo "$GEN1_FILENAME:$GEN1_MD5"
echo "$GEN2_FILENAME:$GEN2_MD5"

# Начать запись/чтение/проверку
COUNTER=1
let REPEAT+=1
while [ $COUNTER -lt $REPEAT ]; do

  echo "==> Cycle $COUNTER [$(date "+%y/%m/%d %T")]"
  echo -n "    * sending $GEN1_FILENAME..."
  sshpass -p$PASSWORD scp -P$PORT $GEN1_FILENAME $USER@$IP:$DST_FILENAME && echo "done"

  echo -n "    * receiving $GEN1_FILENAME..."
  sshpass -p$PASSWORD scp -P$PORT $USER@$IP:$DST_FILENAME $RECV_FILENAME && echo "done"

  CHECK_MD5=$(md5sum $RECV_FILENAME | awk '{print $1;}')
  if [ "$CHECK_MD5" == "$GEN1_MD5" ]; then
    :
  else
    echo "$(date "+%D %T") Error: MD5 mismatch"
    exit 1
  fi

  echo -n "    * sending $GEN2_FILENAME..."
  sshpass -p$PASSWORD scp -P$PORT $GEN2_FILENAME $USER@$IP:$DST_FILENAME && echo "done"
  echo -n "    * receiving $GEN2_FILENAME..."
  sshpass -p$PASSWORD scp -P$PORT $USER@$IP:$DST_FILENAME $RECV_FILENAME && echo "done"

  CHECK_MD5=$(md5sum $RECV_FILENAME | awk '{print $1;}')
  if [ "$CHECK_MD5" == "$GEN2_MD5" ]; then
    :
  else
    echo "$(date "+%D %T") Error: MD5 mismatch"
    exit 1
  fi
  
   let COUNTER+=1
done


